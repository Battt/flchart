import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class FlLineNew extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => FlLineNew1();

}

class FlLineNew1 extends State<FlLineNew>{

  @override
  Widget build(BuildContext context) {

    return AspectRatio(
      aspectRatio: 1.70,
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.only(right: 20.0, left: 12.0, top: 24, bottom: 12),
          child: FlChart(
            chart: LineChart(
              LineChartData(
                backgroundColor: Colors.white70,
                lineTouchData: LineTouchData(
                    getTouchedSpotIndicator: (List<TouchedSpot> spots) {
                      return spots.map((spot) {
                        return TouchedSpotIndicatorData(
                          FlLine(color: Color.fromRGBO(0, 222, 185, 1), strokeWidth: 1),
                          FlDotData(dotSize: 8, dotColor: Color.fromRGBO(0, 222, 185, 1)),
                        );
                      }).toList();
                    },
                    touchTooltipData: TouchTooltipData(
                        tooltipBgColor: Colors.white70,
                        getTooltipItems: (List<TouchedSpot> spots) {
                          return spots.map((spot) {
                            final flSpot = spot.spot;
                            return TooltipItem(
                              '${flSpot.y.toInt() + 1}',
                              const TextStyle(color: Colors.black),
                            );
                          }).toList();
                        }
                    )
                ),
                gridData: FlGridData(
                  show: true,
                  //drawHorizontalGrid: true,
                  getDrawingVerticalGridLine: (value) {
                    if(value == 0 || value == 3 || value == 6) {
                      return const FlLine(
                        color: Colors.grey,
                        strokeWidth: 1,
                      );
                    } else {
                      return const FlLine(
                        color: Colors.white,
                      );
                    }
                  },
                ),
                titlesData: FlTitlesData(
                  show: true,
                  horizontalTitlesTextStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 16
                  ),
                  getHorizontalTitles: (value) {
                    switch(value.toInt()) {
                      case 1: return "Jan";
                      case 3: return "Mar";
                      case 5: return "Jun";
                      case 7: return "Sep";
                      case 9: return "Dec";
                    }
                    return "";
                  },
                  verticalTitlesTextStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                  ),
                  getVerticalTitles: (value) {
                    switch(value.toInt()) {
                      case 0: return "1";
                      case 3: return "4";
                      case 6: return "7";
                    }
                    return "";
                  },
                  verticalTitlesReservedWidth: 28,
                  verticalTitleMargin: 12,
                  horizontalTitleMargin: 8,
                ),
                borderData: FlBorderData(
                    show: false,
                ),
                minX: 0,
                maxX: 10,
                minY: 0,
                maxY: 8,
                lineBarsData: [
                  LineChartBarData(
                    spots: [
                      FlSpot(0, 2),
                      FlSpot(1, 3.3),
                      FlSpot(2.5, 2.8),
                      FlSpot(4, 4),
                      FlSpot(6, 1.6),
                      FlSpot(8, 3.3),
                      FlSpot(9, 5),
                      FlSpot(10, 4.5),
                    ],
                    isCurved: true,
                    colors: [
                      Color.fromRGBO(0, 222, 185, 1)
                    ],
                    barWidth: 3,
                    //isStrokeCapRound: true,
                    dotData: FlDotData(
                      show: false,
                    ),
                    belowBarData: BelowBarData(
                      show: true,
                      colors: [
                        Color.fromRGBO(0, 222, 185, 0.2),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}